import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { enableProdMode } from "@angular/core";

import { AppModule } from "./app/app.module";

if (process.env.ENV === "production") {
  enableProdMode();
} else {
  require("zone.js/dist/long-stack-trace-zone");
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .then(success => console.log("App started successfull"))
  .catch(err => console.error(err));

